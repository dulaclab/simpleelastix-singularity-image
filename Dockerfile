FROM ubuntu:18.04
# https://www.monodevelop.com/download/#fndtn-download-lin
RUN apt update && DEBIAN_FRONTEND=noninteractive apt install -y --no-install-recommends \
  cmake \
  git-core \
  g++ \
  python3 \
  python3-dev \
  r-base \
  r-base-dev \
  && rm -rf /var/lib/apt/lists/*
RUN git clone https://github.com/SuperElastix/SimpleElastix \
  && cd SimpleElastix \
  && git checkout v1.1.0 \
  && mkdir build \
  && cd build \
  && cmake -DITK_GIT_REPOSITORY=https://github.com/InsightSoftwareConsortium/ITK.git ../SuperBuild \
  && make -j 2
RUN cd /SimpleElastix/build/SimpleITK-build/Wrapping/Python/Packaging \
  && ln ../_SimpleITK.so \
  && python3 setup.py install
FROM ubuntu:18.04
RUN apt update && DEBIAN_FRONTEND=noninteractive apt install -y --no-install-recommends \
  python3 \
  r-base \
  && rm -rf /var/lib/apt/lists/*
COPY --from=0 /SimpleElastix/build/SimpleITK-build/Wrapping/R/R_libs/ /usr/lib/R/site-library/
COPY --from=0 /usr/local/lib/python3.6/dist-packages/ /usr/local/lib/python3.6/dist-packages/