#! /usr/bin/env python3

import argparse
import os
import SimpleITK as sitk
import sys

parser = argparse.ArgumentParser()
parser.add_argument("fixed_dir", help="Required. the FULL path to directory containing the fixed images")
parser.add_argument("floating_dir", help="Required. the FULL path to the directory containing the floating images")
args = parser.parse_args()

assert args.fixed_dir is not None, "please provide the path to fixed folder"
assert args.floating_dir is not None, "please provide the path to floating folder"


if not os.path.isdir(args.fixed_dir):
    print("fixed_dir not exist, exiting")
    sys.exit()

if not os.path.isdir(args.floating_dir):
    print("floating_dir not exist, exiting")
    sys.exit()

fixed_dir = args.fixed_dir
floating_dir = args.floating_dir

fixed_map = [os.path.join(fixed_dir, x) for x in os.listdir(fixed_dir) if "DAPI" in x][0]
floating_map = [os.path.join(floating_dir, x) for x in os.listdir(floating_dir) if "DAPI" in x][0]
transform_files = [os.path.join(floating_dir, x) for x in os.listdir(floating_dir) if "DAPI" not in x]


## co-register with DAPI channel
elastixImageFilter = sitk.ElastixImageFilter()
elastixImageFilter.SetFixedImage(sitk.ReadImage(fixed_map))
elastixImageFilter.SetMovingImage(sitk.ReadImage(floating_map))
elastixImageFilter.SetParameterMap(sitk.GetDefaultParameterMap("affine"))
elastixImageFilter.Execute()
sitk.WriteImage(elastixImageFilter.GetResultImage(), os.path.join(fixed_dir, "result_" + os.path.basename(fixed_map)))

transformParameterMap = elastixImageFilter.GetTransformParameterMap()
transformixImageFilter = sitk.TransformixImageFilter()
transformixImageFilter.SetTransformParameterMap(transformParameterMap)

## apply the same transformation to other RNAscope images
population = [os.path.basename(x) for x in transform_files]

for filename in population:
    transformixImageFilter.SetMovingImage(sitk.ReadImage(os.path.join(floating_dir, filename)))
    transformixImageFilter.Execute()
    sitk.WriteImage(transformixImageFilter.GetResultImage(), os.path.join(fixed_dir, "result_" + filename))



