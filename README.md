# SimpleElastix singularity image

how to use the singularity SimpleElastix image

### what is it?

[SimpleElastix](https://simpleelastix.readthedocs.io/Introduction.html) is an image processing library.

use case:

>I’m trying to combine 8 different 5-channel fluorescent images of the same tissue section to create one 40-channel image. I’m doing fairly standard cyclic-immunofluorescene where I stain, image, remove the fluorphores, and repeat. I’m imaging on a Zeiss Axioscan slide scanner, which gives me large tiled and stiched images with 5 fluorescent channels. The images are usually shifted in X-Y in each round of imaging , but there may be small changes in rotation as well (and I can’t rule out small deformations in the tissue between rounds of staining).

>Ideally I’d like to get perfect cell-to-cell aligment across each round of staining for every channel.

>The channels of each imaging round seem well aligned to each other, so what I’d like to do is use the DAPI channel of each 5-channel image to do the alignment (that way any changes are applied to the other 4-channels of each image).

**So, we can use `SimpleElastix` to align DAPI stained cells (the cells shift in each round of imaging).**

### signularity container

The `Dockerfile` is in this repo.

build a docker image

```bash
cd  simpleelastix-singularity-image
docker build . -t simpleelastix:v1.1.0-python-R -m 6g
```


Nathan used `docker2singularity` to export a singularity image, available here:
/n/scratchlfs/informatics/nweeks/simpleelastix:v1.1.0-python-R.sif


### How to use interactively

`singularity` command is only avaiable in the computing node.

```bash
singularity shell simpleelastix:v1.1.0-python-R.sif
python3

Python 3.6.9 (default, Nov  7 2019, 10:44:02) 
[GCC 8.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import SimpleITK
>>> 
>>> 
```


### How to use in a sbatch file

save below to a `align_image.sh`

```bash
#!/bin/bash
#SBATCH -n 1                # Number of cores
#SBATCH -N 1                # Ensure that all cores are on one machine
#SBATCH -t 0-05:00          # Runtime in D-HH:MM, 5 hours
#SBATCH -p serial_requeue   # Partition to submit to
#SBATCH --mem=4000           # Memory pool for all cores (see also --mem-per-cpu)
#SBATCH -o myoutput_%j.out  # File to which STDOUT will be written, %j inserts jobid
#SBATCH -e myerrors_%j.err  # File to which STDERR will be written, %j inserts jobid

singularity exec simpleelastix\:v1.1.0-python-R.sif ./simpleElastix_align.py fixed_folder_path floating_folder_path
```

submit to `Cannon`

```bash
sbatch align_image.sh
```